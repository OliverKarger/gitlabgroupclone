import fetch from 'node-fetch';

export const detect_repos = async (groupID: string, token: string, host?: string): Promise<any[]> => {
    return new Promise(async (resolve, reject) => {
        try {
            const url = `${host}/groups/${groupID}/`;
            console.log(url);
            const projects = await fetch(url + 'projects');
            resolve(projects);
        } catch (e) {
            reject(e);
        }
    })
}