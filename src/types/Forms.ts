export interface ISettingsPrompt {
    gitlabtoken: string;
    gitlabuser: string;
    gitlabgroup: string;
    gitlabgroupdepth: string;
    gitlabhost: string;
};