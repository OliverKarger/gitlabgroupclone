import git from 'isomorphic-git';
import path from 'path';
import fs from 'fs';

/* ! Problems with import-Syntax */
/* tslint:disable */
const http = require('http');

export const git_clone = async (url: string, spath: string): Promise<void> => {
    return new Promise<void>(async (resolve, reject) => {
        try {            
            const rpath = path.resolve(spath);
            await git.clone({fs: fs, http: http, dir: rpath ,url: url});
        } catch (e) {
            reject(e);
        }
        resolve();
    });
}