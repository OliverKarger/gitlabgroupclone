const { Form } =  require('enquirer');
import chalk from 'chalk';
import boxen from 'boxen';

import { detect_repos } from './detector';
import { ISettingsPrompt } from './types/Forms';

const main = async () => {
    return new Promise(async (resolve, reject) => {
        console.log(boxen('GitLab Group Clone', { padding: 1, borderStyle: 'round', align: 'center' }));
        const loginPrompt = new Form({
            name: 'settings',
            message: 'Please Provide the following Information:',
            choices: [
                { name: 'gitlabtoken', message: 'GitLab Access Token', initial: 'abc123' },
                { name: 'gitlabuser', message: 'Your GitLab Username', initial: 'JohnDoeWithNoToe' },
                { name: 'gitlabgroup', message: 'The GitLab Group ID', initial: '1234' },
                { name: 'gitlabgroupdepth', message: 'How deep ?', initial: '1' },
                { name: 'gitlabhost', message: 'GitLab Host', initial: 'https://gitlab.com' }
            ]
        });
        const settings: ISettingsPrompt = await loginPrompt.run();
        const repos = await detect_repos(settings.gitlabgroup, settings.gitlabtoken, settings.gitlabhost);
        console.log(repos);
    });
}

main().catch(e => {
    console.error(e);
});